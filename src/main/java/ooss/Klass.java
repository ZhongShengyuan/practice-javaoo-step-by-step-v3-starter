package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {

    private int id;
    private Student leader;
    private List<Teacher> teacher = new ArrayList<>();
    private List<Student> students = new ArrayList<>();

    public Klass(int id) {
        this.id = id;
    }

    public void assignLeader(Student student) {
        leader = student;

        if (student.getKlass() == null || student.getKlass().getId() != id) {
            System.out.println("It is not one of us.");
        }
        if (teacher.size() != 0) {
            for (int i = 0; i < teacher.size(); i++) {
                System.out.println("I am " + teacher.get(i).getName() + ", teacher of Class " + id + ". I know " + leader.getName() + " become Leader.");
            }
        }

        if (students.size() != 0) {
            for (int i = 0; i < students.size(); i++) {
                System.out.println("I am " + students.get(0).getName() + ", student of Class " + id + ". I know " + leader.getName() + " become Leader.");
            }
        }
    }

    public boolean isLeader(Student student) {
        return leader.getId() == student.getId();
    }

    public void attach(Teacher teacher) {
        this.teacher.add(teacher);
    }

    public void attach(Student student) {
        students.add(student);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }

    @Override
    public boolean equals(Object obj) {
        Klass klass = (Klass) obj;
        return ((Klass) obj).getId() == id;
    }
}
