package ooss;

public class Student extends Person {

    private Klass klass = null;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return this.klass == klass;
    }


    @Override
    public String introduce() {
        if (klass == null)
            return "My name is " + getName() + ". I am " + getAge() + " years old. I am a student.";
        else if (klass.getLeader() != null && klass.isLeader(this))
            return "My name is " + getName() + ". I am " + getAge() + " years old. I am a student. "
                    + "I am the leader of class " + klass.getId() + ".";
        else
            return "My name is " + getName() + ". I am " + getAge() + " years old. I am a student. I am in class " + klass.getId() + ".";
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }
}
