package ooss;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person {

    List<Klass> klass = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public void assignTo(Klass klass) {
        this.klass.add(klass);
    }

    public boolean belongsTo(Klass klass){
        return this.klass.contains(klass);
    }

    public boolean isTeaching(Student student){
        return student.getKlass()!=null && klass.contains(student.getKlass());
    }

    @Override
    public String introduce() {
        if(klass.size()==0)
            return "My name is " + getName() + ". I am " + getAge() + " years old. I am a teacher.";
        if(klass!=null){
            StringBuilder sb =new StringBuilder();
            for (int i = 0; i < klass.size(); i++) {
                sb.append(klass.get(i).getId());
                if(i!=klass.size()-1){
                    sb.append(", ");
                }
            }
            sb.append(".");
            return "My name is " + getName() + ". I am " + getAge() + " years old. I am a teacher. I teach Class "+sb.toString();
        }
        return null;
    }

    public List<Klass> getKlass() {
        return klass;
    }

}
